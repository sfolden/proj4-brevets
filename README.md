# Project 4: Brevet time calculator with Ajax
Author: Shane Folden

I started this project way too late and didn't have time to finish. I'm pretty sure the logic in acp_times is correct but the site doesnt have full functionality currently.

This is a Brevet Controle Time Calculator which uses ACP specifications.

Table: https://rusa.org/octime_alg.html
ACP brevet Calculator: https://rusa.org/octime_acp.html


#how it works:
The way this works is by taking the control distance and dividing by either the min/max max_speeds

ie 60km
max speed at 60 = 34 kph
min speed at 60 = 15 kph

Open time = 60/34    1:45:35
closing time = 60/15 = 4:00:00

If the speed falls over multiple ranges, then each range is calculated and added together

ie 300km

max speed from 0-200 = 34 kph
max speed from 200-400 = 32 kph
min speed from 0-600 = 15 kph

open time = 200/34 + 100/32 hr
close time = 300/15 hr
