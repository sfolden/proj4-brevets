"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#





def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    min_speeds = [(600.0, 15.0), (1000.0, 11.428), (1300, 13.33)]
    max_speeds = [(200.0, 34.0), (400.0, 32.0), (600.0, 30.0), (1000.0, 28.0), (1300.0, 26.0)]

    if control_dist_km < 0:
        return "Control Distance must be >= 0"

    # if contrl dist is greater than brev dist, control dist = brev distance
    if control_dist_km >= brevet_dist_km:
        control_dist_km = brevet_dist_km

    time_total = 0.0
    travel_total = 0.0

    for val in max_speeds:
        dist, speed = val
        if control_dist_km >=dist:
            time_total = time_total + ((dist-travel_total) / speed)
            travel_total = dist
        else:
            time_total = time_total + ((control_dist_km-travel_total)/speed)
            break

    hrs = int(time_total)
    min = int((time_total - hours)*60)
    sec = round(((time_total-hours)*60 - min)*60, 6)
    formatted_time = arrow.get(brevet_start_time + ":00", "YYYY-MM-DD HH:mm:ss")
    formatted_time = formatted_time.shift(hours=+hrs, minutes=+min, seconds=+sec)



    return formatted_time.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    min_speeds = [(600.0, 15.0), (1000.0, 11.428), (1300, 13.33)]
    max_speeds = [(200.0, 34.0), (400.0, 32.0), (600.0, 30.0), (1000.0, 28.0), (1300.0, 26.0)]


    if control_dist_km < 0:
        return "Control Distance must be >= 0"
    if control_dist_km == 0:
        formatted_time = arrow.get(brevet_start_time + ":00", "YYYY-MM-DD HH:mm:ss")
        formatted_time = formatted_time.shift(hours=+1)
        return formatted_time.isoformat()


    if control_dist_km >= brevet_dist_km:
        control_dist_km = brevet_dist_km

    time_total = 0.0
    travel_total = 0.0

    for val in max_speeds:
        dist, speed = val
        if control_dist_km >=dist:
            time_total = time_total + ((dist-travel_total) / speed)
            travel_total = dist
        else:
            time_total = time_total + ((control_dist_km-travel_total)/speed)
            break

    hrs = int(time_total)
    min = int((time_total - hours)*60)
    sec = round(((time_total-hours)*60 - min)*60, 6)
    formatted_time = arrow.get(brevet_start_time + ":00", "YYYY-MM-DD HH:mm:ss")
    formatted_time = formatted_time.shift(hours=+hrs, minutes=+min, seconds=+sec)
    return formatted_time.isoformat()
